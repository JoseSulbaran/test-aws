// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const docClient = new AWS.DynamoDB({apiVersion: '2012-10-08'});

exports.lambdaHandler = async (event, context) => {
    var message =  'hello world';
    try {
        var params = {
            TableName : DynamoTable,
            ProjectionExpression:"#yr, #Nombre, #Nombre, #Gmail, #Telefono, #Sexo, #Documento, #CreatedAt",
            KeyConditionExpression: "#yr = :yyyy",
            ExpressionAttributeNames:{
                "#yr": "Year",
                "#Nombre": "Nombre",
                "#Apellido": "Apellido",
                "#Documento": "Documento",
                "#Gmail": "Gmail",
                "#Sexo": "Sexo",
                "#Telefono": "Telefono",
                "#CreatedAt": "CreatedAt"
            },
            ExpressionAttributeValues: {
                ":yyyy": 2020,
            }
        };
        docClient.get(params, function(err, data) {
            if (err) {
                response = {
                    'statusCode': 200,
                    'body': JSON.stringify({
                        message: 'error',
                        err: err
                    })
                };
            } else {
                response = {
                    'statusCode': 200,
                    'body': JSON.stringify({
                        message: 'lista usuarios',
                        data: data
                    })
                };
            }
        });
                // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'registro exitoso',
                result: data
            })
        };
    } catch (err) {
        response = {
            'statusCode': 400,
            'body': JSON.stringify({
                message: 'error al registar la data',
                err: err
            })
        }
    }
    return response
};
