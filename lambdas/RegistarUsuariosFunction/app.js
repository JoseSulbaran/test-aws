// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const docClient = new AWS.DynamoDB({apiVersion: '2012-10-08'});

exports.lambdaHandler = async (event, context) => {
    var message =  'hello world';
    var { nombre, apellido, gmail, telefono, sexo, documento, DynamoTable } = event;

    try {
        var params = {
          TableName: DynamoTable,
          Item: {
            'Nombre' : nombre,
            'Apellido' : apellido,
            'Gmail' : gmail, 
            'Telefono' : telefono,
            'Sexo' : sexo, 
            'Documento' : documento,
            'Year':2020,
            'CreatedAt': new  Date()
          }
        };
        0.put(params, function(err, data) {
            if (err) {
                response = {
                    'statusCode': 200,
                    'body': JSON.stringify({
                        message: 'error al registar la data',
                        err: err
                    })
                }
            } else {
                response = {
                    'statusCode': 200,
                    'body': JSON.stringify({
                        message: 'registro exitoso',
                        data: data
                    })
                }
            }
        });

    } catch (err) {
        response = {
            'statusCode': 400,
            'body': JSON.stringify({
                message: 'error al registar la data',
                err: err
            })
        }
    }
    return response
};
